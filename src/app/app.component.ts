import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'model';

  isReadMore = true;

  process1: boolean = true;
  process2: boolean;
  process3: boolean;

  showText() {
     this.isReadMore = !this.isReadMore
  }

  previous() {
    if (this.process1 == true) {
      this.process1 = false;
      this.process2 = false;
      this.process3 = false;
      return;
    }
    else if (this.process2 == true) {
      this.process1 = true;
      this.process2 = false;
      this.process3 = false;
      return;
    }
    else if (this.process3 == true) {
      this.process1 = false;
      this.process2 = true;
      this.process3 = false;
      return;
    }
  }



  next() {
    if (this.process1 == true) {
      this.process1 = false;
      this.process2 = true;
      this.process3 = false;
      return;
    }
    else if (this.process2 == true) {
      this.process1 = false;
      this.process2 = false;
      this.process3 = true;
      return;
    }
    else if (this.process3 == true) {
      this.process1 = false;
      this.process2 = false;
      this.process3 = false;
      return;
    }
  }


  




}
